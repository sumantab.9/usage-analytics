package com.example.appusageanalytics

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import com.example.appusageanalytics.services.BaseResponse
import com.example.appusageanalytics.services.RequestSignup
import com.example.appusageanalytics.services.WebServices
import com.pixplicity.easyprefs.library.Prefs
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_update_profile.*

class UpdateProfileActivity : AppCompatActivity() {
    val webService = WebServices.create()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_profile)
        setSupportActionBar(toolbar2)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        txtUsername.setText(Prefs.getString("profile.email"))
        txtPassword.setText(Prefs.getString("profile.pwd"))
        btnSave.setOnClickListener {
            btnSave.isEnabled = false
            Log.e("Saving profile", txtUsername.text?.toString() + "::" + txtPassword.text?.toString())
            val hUpdateProfile = webService.updateProfile(RequestSignup(txtUsername.text.toString(), txtPassword.text.toString(), Integer.parseInt(Prefs.getString("profile.id"))))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object:Observer<BaseResponse>{
                    override fun onSubscribe(d: Disposable) {

                    }

                    override fun onNext(t: BaseResponse) {
                        Toast.makeText(this@UpdateProfileActivity, t.msg, Toast.LENGTH_SHORT).show()
                        Prefs.putString("profile.email", txtUsername.text.toString())
                        Prefs.putString("profile.pwd", txtPassword.text.toString())
                    }

                    override fun onError(e: Throwable) {
                        Toast.makeText(this@UpdateProfileActivity, "Cannot update profile. Please try again", Toast.LENGTH_SHORT).show()
                    }

                    override fun onComplete() {
                        btnSave.isEnabled = true
                    }

                })
        }
        btnLogout.setOnClickListener {
            Prefs.clear()
            finishAffinity()
            startActivity(Intent(this@UpdateProfileActivity, LoginActivity::class.java))
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home){
            super.onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}