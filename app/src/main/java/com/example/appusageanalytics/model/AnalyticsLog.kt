package com.example.appusageanalytics.model

import com.orm.SugarRecord

class AnalyticsLog : SugarRecord<AnalyticsLog> {

    lateinit var packageName: String
    lateinit var applicationName: String
    lateinit var latExecutionTime: String

    constructor() {
    }

    constructor(packageName: String, applicationName: String, latExecutionTime: String) : super() {
        this.packageName = packageName
        this.applicationName = applicationName
        this.latExecutionTime = latExecutionTime
    }
}