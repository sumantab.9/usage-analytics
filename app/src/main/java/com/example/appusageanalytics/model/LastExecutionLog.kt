package com.example.appusageanalytics.model

import com.orm.SugarRecord

class LastExecutionLog : SugarRecord<LastExecutionLog> {

    lateinit var packageName: String
    var totalTime: Long = 0
    lateinit var latExecutionTime: String

    constructor() {
    }

    constructor(packageName: String, totalTime: Long, latExecutionTime: String) : super() {
        this.packageName = packageName
        this.totalTime = totalTime
        this.latExecutionTime = latExecutionTime
    }
}