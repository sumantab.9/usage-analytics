package com.example.appusageanalytics

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import com.example.appusageanalytics.services.RequestAddTip
import com.example.appusageanalytics.services.ResponseTip
import com.example.appusageanalytics.services.WebServices
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_add_tips.*

class AddTipsActivity : AppCompatActivity() {
    private val webService = WebServices.create()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_tips)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        btnAddTip.setOnClickListener {
            onAddTip()
        }
    }

    private fun onAddTip() {
        if(txtTipTitle.text.isNullOrEmpty()){
            Toast.makeText(this@AddTipsActivity, "Please enter Tip title", Toast.LENGTH_SHORT).show()
            return
        }
        if(txtTipDetails.text.isNullOrEmpty()){
            Toast.makeText(this@AddTipsActivity, "Please enter Tip description", Toast.LENGTH_SHORT).show()
            return
        }
        btnAddTip.isEnabled = false
        val hTip = webService.createTip(RequestAddTip(txtTipTitle.text.toString(),txtTipDetails.text.toString()))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object:Observer<ResponseTip>{
                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(t: ResponseTip) {
                    Toast.makeText(this@AddTipsActivity, "Tip added successfully", Toast.LENGTH_SHORT).show()
                    val resultIntent = Intent()
                    resultIntent.putExtra("id", t.data.toString())
                    resultIntent.putExtra("title", txtTipTitle.text.toString())
                    resultIntent.putExtra("details", txtTipDetails.text.toString())
                    setResult(RESULT_OK, resultIntent)
                    finish()
                }

                override fun onError(e: Throwable) {

                }

                override fun onComplete() {
                    btnAddTip.isEnabled = false
                }

            })
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home){
            super.onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}