package com.example.appusageanalytics

import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.rvalerio.fgchecker.AppChecker
import java.util.*
import android.app.NotificationManager

import android.app.NotificationChannel
import android.os.Build
import com.example.appusageanalytics.model.AnalyticsLog
import com.example.appusageanalytics.model.LastExecutionLog
import com.orm.SugarRecord
import java.text.SimpleDateFormat
import android.content.pm.PackageManager

import android.app.AppOpsManager
import android.app.PendingIntent
import android.content.Context

import android.content.pm.ApplicationInfo
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.TaskStackBuilder
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.appusageanalytics.containers.TipsRecyclerViewAdapter
import com.example.appusageanalytics.services.ResponseTips
import com.example.appusageanalytics.services.Tip
import com.example.appusageanalytics.services.WebServices
import com.pixplicity.easyprefs.library.Prefs
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private val webService = WebServices.create()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (!checkIfUsageAccessAllowed()) {
            requestUsageStatsPermission()
        }
        rcViewTips.setHasFixedSize(true)
        rcViewTips.layoutManager = LinearLayoutManager(this@MainActivity)
        rcViewTips.adapter = TipsRecyclerViewAdapter()
        fetchTips()
        //Reset all entry
        SugarRecord.deleteAll(LastExecutionLog::class.java)
        val t = Timer()
        t.scheduleAtFixedRate(object : TimerTask() {
            override fun run() {
                getAppUsage()
            }
        }, 0, 2_500)
        switchShowNotification.isChecked = Prefs.getBoolean("show_notification", false)
        txtMaxLimit.setText(Prefs.getInt("time", 5).toString())
        switchShowNotification.setOnCheckedChangeListener { _, isChecked ->
            Prefs.putBoolean("show_notification", isChecked)
        }
        btnSaveTime.setOnClickListener {
            Prefs.putInt("time", txtMaxLimit.text.toString().toInt())
            txtMaxLimit.clearFocus()
            Toast.makeText(this@MainActivity, "Timer saved", Toast.LENGTH_SHORT).show()
        }
        Toast.makeText(this@MainActivity, Prefs.getString("profile.role", "not_admin"), Toast.LENGTH_SHORT).show()
        if(Prefs.getString("profile.role", "not_admin") == "admin"){
            btnAddTips.visibility = View.VISIBLE
        }
        btnAddTips.setOnClickListener {
            openAddTip()
        }
        btnProfile.setOnClickListener {
            startActivity(Intent(this@MainActivity, UpdateProfileActivity::class.java))
        }
    }

    private fun openAddTip(){
        startActivityForResult(Intent(this@MainActivity, AddTipsActivity::class.java), 2000)
    }

    private fun fetchTips() {
        val hTips = webService.fetchTips()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : Observer<ResponseTips> {
                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(t: ResponseTips) {
                    Log.e("AppName", "Fetched ${t.data.size} tips")
                    (rcViewTips.adapter as TipsRecyclerViewAdapter).tips = t.data
                }

                override fun onError(e: Throwable) {
                    e.printStackTrace()
                }

                override fun onComplete() {

                }

            })
    }

    private fun checkIfUsageAccessAllowed(): Boolean {
        return try {
            val packageManager: PackageManager = packageManager
            val applicationInfo = packageManager.getApplicationInfo(packageName, 0)
            val appOpsManager = getSystemService(Context.APP_OPS_SERVICE) as AppOpsManager
            val mode = appOpsManager.checkOpNoThrow(
                AppOpsManager.OPSTR_GET_USAGE_STATS,
                applicationInfo.uid,
                applicationInfo.packageName
            )
            mode == AppOpsManager.MODE_ALLOWED
        } catch (e: PackageManager.NameNotFoundException) {
            false
        }
    }

    private fun showNotification(payload: String, subtitle: String) {
        val notificationManager = NotificationManagerCompat.from(this)
        if (Build.VERSION.SDK_INT >= 26) {
            val channel = NotificationChannel(
                "MyChannel",
                "MyChannelName",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            channel.description = "YOUR_NOTIFICATION_CHANNEL_DESCRIPTION"
            notificationManager.createNotificationChannel(channel)
            val resultIntent = Intent(this, MainActivity::class.java)
            val pendingIntent = TaskStackBuilder.create(this@MainActivity).run {
                addNextIntentWithParentStack(resultIntent)
                getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
            }
            val builder = NotificationCompat.Builder(this, "MyChannel")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(payload)
                .setContentText(payload)
                .setContentIntent(pendingIntent)
                .setStyle(NotificationCompat.BigTextStyle()
                    .bigText(subtitle))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            with(NotificationManagerCompat.from(this)) {
                notify(15_000, builder.build())
            }
        }
    }

    private fun requestUsageStatsPermission() {
        startActivity(Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS))
    }

    fun getAppUsage() {
        if(!checkIfUsageAccessAllowed()){
            Log.e("AppName", "Not allowed")
            return;
        }
        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS")
        val appChecker = AppChecker()
        val packageName = appChecker.getForegroundApp(this@MainActivity)
        val pm = applicationContext.packageManager
        val ai = pm.getApplicationInfo(packageName, 0)
        val appName = pm.getApplicationLabel(ai).toString()
        val analyticsEntry =
            AnalyticsLog(packageName, appName, sdf.format(Date()))
        analyticsEntry.save()
        val lastEntryRes = SugarRecord.listAll(LastExecutionLog::class.java)
        if (lastEntryRes.size > 0) {
            val lastEntry = lastEntryRes.first()
            if (lastEntry.packageName == packageName) {
                val lastExecutionTime = lastEntry.latExecutionTime
                val lastExecutionDateTime = sdf.parse(lastExecutionTime)
                val totalTimeDifference = (Date().time - lastExecutionDateTime!!.time) / 1_000
                Log.d("AppName", "$appName running for $totalTimeDifference seconds")
                lastEntry.totalTime = totalTimeDifference
                lastEntry.save()
                Log.e("AppName", "Time: ${Prefs.getInt("time", 5)}")
                if (totalTimeDifference > 60 * Prefs.getInt("time", 5)) {
                    if (Prefs.getBoolean("show_notification", false)) {
                        if(packageName != getPackageName()){
                            showNotification("You have been on $appName for too long", "Take a break. You might get addicted.")
                        }
                    }
                }
            } else {
                SugarRecord.deleteAll(LastExecutionLog::class.java)
                val analyticsTopEntry = LastExecutionLog(packageName, 0, sdf.format(Date()))
                analyticsTopEntry.save()
            }
        } else {
            val analyticsTopEntry = LastExecutionLog(packageName, 0, sdf.format(Date()))
            analyticsTopEntry.save()
            Log.d("AppName", "Created new entry")
        }
        Log.d("AppName", "Save record")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode == 2000 && resultCode == RESULT_OK){
            val tip = Tip(data?.getStringExtra("id")!!, data.getStringExtra("title")!!, data.getStringExtra("details")!!)
            Log.e("AppName", "New tip added")
            (rcViewTips.adapter as TipsRecyclerViewAdapter).addTip(tip)
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}