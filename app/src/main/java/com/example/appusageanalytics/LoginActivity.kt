package com.example.appusageanalytics

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.example.appusageanalytics.services.RequestLogin
import com.example.appusageanalytics.services.ResponseLogin
import com.example.appusageanalytics.services.WebServices
import com.pixplicity.easyprefs.library.Prefs
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    private var webService = WebServices.create()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        setupCallbacks()
        if(Prefs.getString("profile.id", "X") !== "X"){
            startMainActivity()
        }
    }

    private fun startMainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_TASK_ON_HOME
        startActivity(intent)
    }

    private fun startSignupActivity() {
        val intent = Intent(this, SignupActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_TASK_ON_HOME
        startActivity(intent)
    }

    private fun login() {
        if (txtUsername.text.isNullOrEmpty()) {
            Toast.makeText(this@LoginActivity, "Please enter username", Toast.LENGTH_SHORT).show()
            return
        }
        if (txtPassword.text.isNullOrEmpty()) {
            Toast.makeText(this@LoginActivity, "Please enter password", Toast.LENGTH_SHORT).show()
            return
        }
        btnLogin.isEnabled = false
        val hLoginRequest =
            webService.login(RequestLogin(txtUsername.text.toString(), txtPassword.text.toString()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object:Observer<ResponseLogin> {
                    override fun onNext(t: ResponseLogin) {
                        if(t.code == 200){
                            Toast.makeText(this@LoginActivity, t.msg, Toast.LENGTH_SHORT).show()
                            Prefs.putString("profile.id", t.data.id)
                            Prefs.putString("profile.email", t.data.email)
                            Prefs.putString("profile.role", t.data.role)
                            Prefs.putString("profile.pwd", t.data.pwd)
                            startMainActivity()
                        }else{
                            Toast.makeText(this@LoginActivity, t.msg, Toast.LENGTH_SHORT).show()
                        }
                        Log.e("AppName", t.msg)
                    }

                    override fun onError(e: Throwable) {
                        btnLogin.isEnabled = true
                        e.printStackTrace()
                        Toast.makeText(this@LoginActivity, "Invalid login credentials", Toast.LENGTH_SHORT).show()
                    }

                    override fun onComplete() {
                        btnLogin.isEnabled = true
                    }

                    override fun onSubscribe(d: Disposable) {

                    }

                })
    }

    private fun setupCallbacks() {
        btnLogin.setOnClickListener {
            login()
        }
        btnSignup.setOnClickListener {
            startSignupActivity()
        }
    }
}