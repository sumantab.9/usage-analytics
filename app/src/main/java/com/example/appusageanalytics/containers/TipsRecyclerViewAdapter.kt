package com.example.appusageanalytics.containers

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.Toast
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.example.appusageanalytics.R
import com.example.appusageanalytics.containers.TipsRecyclerViewAdapter.ViewHolder
import com.example.appusageanalytics.services.BaseResponse
import com.example.appusageanalytics.services.Tip
import com.example.appusageanalytics.services.WebServices
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.pixplicity.easyprefs.library.Prefs
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.tip_layout.view.*

class TipsRecyclerViewAdapter : RecyclerView.Adapter<ViewHolder>() {
    lateinit var context: Context
    private val webService = WebServices.create()
    var tips = arrayListOf<Tip>()
        set(value) {
            setTips(value)
        }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val txtTitle: AppCompatTextView = itemView.txtTipTitle
        private val txtContent: AppCompatTextView = itemView.txtTipContent
        val deleteButton: ImageButton = itemView.deleteButton

        init {
            if (Prefs.getString("profile.role", "not_admin") == "admin") {
                deleteButton.visibility = View.VISIBLE
            }
        }

        fun bind(tip: Tip) {
            txtTitle.text = tip.title
            txtContent.text = tip.details
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.tip_layout, parent, false)
        context = parent.context
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(tips[position])
        holder.deleteButton.setOnClickListener {
            MaterialAlertDialogBuilder(context)
                .setMessage("Do you want to delete this tip?")
                .setNegativeButton("Dismiss") { dialog, which ->
                    // Respond to negative button press
                }
                .setPositiveButton("Delete") { dialog, which ->
                    val _position = position;
                    val hDelete = webService.deleteTip(tips[position])
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(object : Observer<BaseResponse> {
                            override fun onSubscribe(d: Disposable) {

                            }

                            override fun onNext(t: BaseResponse) {
                                Toast.makeText(context, "Deleted successfully", Toast.LENGTH_SHORT)
                                    .show()
                                tips.removeAt(_position)
                                notifyItemRemoved(_position)
                            }

                            override fun onError(e: Throwable) {

                            }

                            override fun onComplete() {

                            }

                        })
                }
                .show()
        }
    }

    override fun getItemCount(): Int {
        return tips.size
    }

    public fun addTip(tip: Tip) {
        tips.add(tip)
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun setTips(_tips: List<Tip>) {
        tips.clear()
        tips.addAll(_tips)
        notifyDataSetChanged()
    }
}