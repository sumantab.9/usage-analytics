package com.example.appusageanalytics

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import com.example.appusageanalytics.services.RequestSignup
import com.example.appusageanalytics.services.ResponseSignup
import com.example.appusageanalytics.services.WebServices
import com.pixplicity.easyprefs.library.Prefs
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_signup.*
import kotlinx.android.synthetic.main.activity_signup.txtUsername

class SignupActivity : AppCompatActivity() {
    private val webService = WebServices.create()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""
        btnSignup.setOnClickListener {
            signup()
        }
    }

    private fun signup(){
        if(txtUsername.text.isNullOrEmpty()){
            Toast.makeText(this@SignupActivity, "Enter username", Toast.LENGTH_SHORT).show()
            return
        }
        if(txtPassword.text.isNullOrEmpty()){
            Toast.makeText(this@SignupActivity, "Enter password", Toast.LENGTH_SHORT).show()
            return
        }
        btnSignup.isEnabled = false
        var request = RequestSignup(txtUsername.text.toString(), txtPassword.text.toString(), -1)
        Log.e("Request", request.toString())
        val hSignup = webService.signup(request)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object:Observer<ResponseSignup>{
                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(t: ResponseSignup) {
                    if(t.code == 200) {
                        Toast.makeText(this@SignupActivity, "Signup successful", Toast.LENGTH_SHORT)
                            .show()
                        val intent = Intent(this@SignupActivity, LoginActivity::class.java)
                        finishAffinity()
                        startActivity(intent)
                    }else{
                        Toast.makeText(this@SignupActivity, t.msg, Toast.LENGTH_SHORT)
                            .show()
                    }
                }

                override fun onError(e: Throwable) {
                    e.printStackTrace()
                }

                override fun onComplete() {
                    btnSignup.isEnabled = true
                }

            })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            val intent = Intent(this@SignupActivity, LoginActivity::class.java)
            finishAffinity()
            startActivity(intent)
        }
        return super.onOptionsItemSelected(item)
    }
}