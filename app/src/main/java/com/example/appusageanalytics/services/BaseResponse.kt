package com.example.appusageanalytics.services

open class BaseResponse(val code:Int, val msg: String)