package com.example.appusageanalytics.services

open class Tip(val id: String, val title: String, val details: String)