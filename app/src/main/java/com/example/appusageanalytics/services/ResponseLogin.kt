package com.example.appusageanalytics.services

class ResponseLogin(code: Int, msg: String, val data: LoginData): BaseResponse(code, msg)



class LoginData(val id: String, val email: String, val pwd: String, val role: String)