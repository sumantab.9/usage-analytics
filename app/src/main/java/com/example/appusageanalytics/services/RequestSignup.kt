package com.example.appusageanalytics.services

class RequestSignup(val email: String, val pwd: String, val id: Int) {
    override fun toString(): String {
        return "Email: $email, Password: $pwd, ID: $id"
    }
}