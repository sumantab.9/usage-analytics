package com.example.appusageanalytics.services

import io.reactivex.Observable
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import java.util.concurrent.TimeUnit

interface WebServices {
    @POST("login.php")
    fun login(@Body request: RequestLogin) : Observable<ResponseLogin>
    @POST("signup.php")
    fun signup(@Body request: RequestSignup) : Observable<ResponseSignup>
    @POST("update_profile.php")
    fun updateProfile(@Body request: RequestSignup) : Observable<BaseResponse>
    @GET("list_tips.php")
    fun fetchTips() : Observable<ResponseTips>
    @POST("tips_create.php")
    fun createTip(@Body request: RequestAddTip) : Observable<ResponseTip>
    @POST("delete_tips.php")
    fun deleteTip(@Body request: Tip) : Observable<BaseResponse>
    companion object {
        fun create(): WebServices {
            val httpClient = OkHttpClient.Builder()
            httpClient.callTimeout(300, TimeUnit.SECONDS);
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .baseUrl("https://antiaddiction.herokuapp.com/")
                .build()
            return retrofit.create(WebServices::class.java)
        }
    }
}
