package com.example.appusageanalytics;

import android.content.ContextWrapper;

import com.orm.SugarApp;
import com.pixplicity.easyprefs.library.Prefs;

public class AnalyticsApp extends SugarApp {
    @Override
    public void onCreate() {
        super.onCreate();
        new Prefs.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(getPackageName())
                .setUseDefaultSharedPreference(true)
                .build();
    }
}
