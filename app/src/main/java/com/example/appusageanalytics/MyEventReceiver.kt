package com.example.appusageanalytics

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log

class MyEventReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        // This method is called when the BroadcastReceiver is receiving an Intent broadcast.
        Log.d("AppName", "onReceive")
    }
}